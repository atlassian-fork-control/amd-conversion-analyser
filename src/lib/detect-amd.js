'use strict';
const _ = require('lodash');
const acorn = require('acorn');
const walk = require('acorn/dist/walk');

function reallyParse(source) {
  try {
    return acorn.parse(source, {
      ecmaVersion: 8,
      ecmaScript: 8,
      allowTrailingComma: true,
      allowReturnOutsideFunction: true,
      allowImportExportEverywhere: true,
      allowHashBang: true
    });
  } catch (ex) {
    return acorn.parse(source, {
      ecmaVersion: 5,
      allowReturnOutsideFunction: true,
      allowImportExportEverywhere: true,
      allowHashBang: true
    });
  }
}

function isAMDDefine(node) {
    return node.type === 'CallExpression'
        && node.callee
        && node.callee.type === 'Identifier'
        && node.callee.name === 'define';
}


function isAMDRequire(node) {
    return node.type === 'CallExpression'
        && node.callee
        && node.callee.type === 'Identifier'
        && node.callee.name === 'require';
}

function asAMDNode(node, type) {
    let moduleName = node.arguments[0];
    let deps = node.arguments[1];
    let factory = node.arguments[2];

    if (moduleName.type === 'ArrayExpression') {
        factory = deps;
        deps = moduleName;
        moduleName = undefined;
    }
    if (!deps && !factory) {
        factory = moduleName;
        deps = undefined;
        factory = undefined;
    }
    else if (deps.type !== 'ArrayExpression') {
        factory = deps;
        deps = [];
    }
    if (!deps) {
        deps = [];
    }
    else if (deps.type === 'ArrayExpression') {
        deps = _.map(deps.elements, el => el.value);
    }

    const amd = {
        node,
        type,
        deps
    };

    if (moduleName) {
        amd.name = moduleName.value;
    }
    return amd;
}

function shouldCountRequire(maybeRequire) {
    return maybeRequire.deps.length > 0;
}

function detectAMD(content) {
    const nodes = [];
    const ast = (typeof content === 'string') ? reallyParse(content) : content;

    walk.ancestor(ast, {
        'CallExpression': function(node) {
            if (isAMDDefine(node)) {
                nodes.push(asAMDNode(node, 'define'));
            }
            else if (isAMDRequire(node)) {
                let maybeRequire = asAMDNode(node, 'require');
                if (shouldCountRequire(maybeRequire)) {
                    nodes.push(maybeRequire);
                }
            }
        }
    });

    return nodes;
}

module.exports = detectAMD;